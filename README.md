# LogDna Transport for Pino

This module provides a "transport" for pino that forwards messages to the LogDNA log service through official LogDna NPM module.

The module will echo the received logs or work silently.

**Heavily** inspired by https://www.npmjs.com/package/pino-logdna-formatter